import React from 'react';
import './CardList.css'
import Card from './Card';

class CardList extends React.Component {

    render () {

        return (
            <section>
                <Card title='One'
                content='OneContent'
                buttonText='OneButtonText'
                id='1'
                />
                <Card title='Two'
                content='TwoContent'
                buttonText='TwoButtonText'
                id='2'
                />
                <Card title='Three'
                content='ThreeContent'
                buttonText='ThreeButtonText'
                id='3'
                />
            </section>
        )

    }

}

export default CardList;

// Door
// Tijmen, Maisie en Marlène