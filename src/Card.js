import React from 'react';
import './Card.css'

class Card extends React.Component {
    constructor (props) {
        super(props);
        this.state = {
            id: props.id
        };
    }

    onCardClicked = () => {
        console.log('no id yet ' + this.state.id);
    }
    
    render () { 
        return (
            <article className="card">
                <header className="card__header">
                    <h2> {this.props.title || 'Standaard info'} </h2>
                </header>
                <section className="card__content">
                    <p> {this.props.content || 'Standaard content'} </p>
                </section>
                <button className="card__button" onClick = {this.onCardClicked}> 
                    {this.props.buttonText || 'Standaard buttonText'}
                </button>
            </article>
        )
    }

}
export default Card;

// Door
// Tijmen, Maisie en Marlène